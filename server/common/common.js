const path = require('path')
const fs = require('fs')
const csvHeaders = require('csv-headers')


const validateFileExtension = (file, regex) => {
    return new Promise((resolve, reject) => {
        if (!file.name.match(regex)) {
           reject('Only .csv files')
        } else {
            resolve()
        }
    })
}

const uploadFile = (file, name) => {
    return new Promise((resolve, reject) => {
        const newDir = path.join(__dirname, '../../almanac', name)
        fs.mkdir(newDir, () => {
            const filePath = path.join(newDir, 'source.csv')
            file.mv(filePath, function(err) {
                console.log(err)
                if (err) {
                    return reject(err)
                } else {
                    return resolve(filePath)
                }
            })
        })
        
    })
}

const emptyAlmanac = () => {

}

const extractHeaders = (filePath) => {
    return new Promise((resolve, reject) => {
        let options = {
            file      : filePath,
            delimiter : ';'
        }
         
        csvHeaders(options, function(err, headers) {
            if (err) {
                return reject(err)
            } else {
                return resolve(headers)
            }
        })
    })
}

const extractBody = (options) => {
    return new Promise ((resolve, reject) => {
        resolve()
    })
}

module.exports = {
    uploadFile: uploadFile,
    extractHeaders: extractHeaders,
    validateFileExtension: validateFileExtension,
    extractBody: extractBody
}