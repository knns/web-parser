const path = require('path');
const fs = require('fs');
const csv2json = require('csvtojson');
const csv = require("fast-csv");
const csvHeaders = require('csv-headers')
var archiver = require('archiver');

class FinancialStateFactory {
    constructor() {
        this.rawData = []
        this.data = []
    }

    io(object) {
        this.uuid = object.uuid
        this.fileName = object.fileName
        this.companyList = object.companyList
        this.code = object.code
    }

    extractHeaders () {
        return new Promise((resolve, reject) => {
            let options = {
                file : path.join(__dirname, '../../almanac', this.uuid, 'source.csv'),
                delimiter: ';'
            }
             
            csvHeaders(options, function(err, headers) {
                if (err) {
                    return reject(err)
                } else {
                    return resolve(headers)
                }
            })
        })
    }

    readRawUsingOptions(options) {
        return new Promise((resolve, reject) => {
            const rawData = []
            this.extractHeaders().then((headers) => {
                const csvOptions  = {
                    delimiter: ';',
                    trim: true,
                    headers: headers
                }
                csv2json(csvOptions)
                .fromFile(path.join(__dirname, '../../almanac', this.uuid, 'source.csv'))
                .on('json', (jsonObject) => {
                    rawData.push(jsonObject);
                }).on('done', (err, success) => {
                    if (err) {
                        reject(err)
                    } else {
                        this.rawData = rawData
                        resolve()
                    }
                });
            }).catch((err) => {
                reject(err)
            })
        })
    }

    optimize() {
        return new Promise((resolve, reject) => {
            let result = [];
            this.companyList.forEach((propertie) => {
                let sum = 0;
                result.push({
                    company: propertie,
                    accounts: this.rawData.map((rawElement) => {
                        return {
                            code: rawElement[this.code],
                            balance: Number(rawElement[propertie].split(',').join(''))
                        }
                    }).filter((rawElement) => {
                        if (rawElement.code.startsWith('7') || rawElement.code.startsWith('9')) {
                            console.log('excluded ' + rawElement.code + ' from ' + propertie + ', cause: starts with ' + rawElement.code[0])
                        } else {
                            sum += rawElement.balance;
                            if (rawElement.balance != 0) {
                                return rawElement;
                            }
                        }
                    })
                })
                if (sum.toFixed(2) != 0) {
                    reject(propertie + " :not zero, instead get " + sum)
                }
            })
            this.data = result;
            return resolve()
        })
    }

    writeCSV() {
        return new Promise((resolve, reject) => {
            const errorList = []
            try {
                //console.log('this shit',JSON.stringify(this.data, null, 2))
                this.data.forEach((element) => {
                    const csvStream = csv.format({ headers: false })
                    const filePath = path.join( __dirname, '../../almanac/', this.uuid, element.company + ".csv").toString()
                    const writableStream = fs.createWriteStream(filePath)
                    
                    csvStream.pipe(writableStream);
                    element.accounts.forEach((account) => {
                        csvStream.write(account);
                    });
                    csvStream.end();

                    writableStream.on("finish", function () {

                    });
                    
                })
            } catch (err) {
                console.log(err)
                errorList.push(err)
            }
            if(errorList.length > 0) {
                reject(errorList)
            } else {
                resolve()
            }
        })
    }

    generateZIP() {
        return new Promise(( resolve, reject ) => {
            const filePath = path.join( __dirname, '../../almanac/', this.uuid).toString()
            console.log('asdasdpaht',path.join(filePath,  this.fileName + '.zip'))
            const output = fs.createWriteStream(path.join(filePath,  this.fileName + '.zip').toString())
            const archive = archiver('zip', {
              zlib: { level: 9 } // Sets the compression level.
            });
             
            // listen for all archive data to be written
            // 'close' event is fired only when a file descriptor is involved
            output.on('close', function() {
              console.log(archive.pointer() + ' total bytes');
              console.log('archiver has been finalized and the output file descriptor has closed.');
              resolve()
            });
             
            // This event is fired when the data source is drained no matter what was the data source.
            // It is not part of this library but rather from the NodeJS Stream API.
            // @see: https://nodejs.org/api/stream.html#stream_event_end
            output.on('end', function() {
              console.log('Data has been drained');
    
            });
            // good practice to catch warnings (ie stat failures and other non-blocking errors)
            archive.on('warning', function(err) {
              console.log(err)
            });
             
            archive.on('error', function(err) {
              reject (err);
            });
             
            archive.pipe(output);
            
            this.companyList.forEach((company) => {
                archive.file(path.join(filePath, company + '.csv'), { name: company + '.csv' });
            })

            archive.finalize();
        })
    }
}

Number.prototype.countDecimals = function () {
    if (Math.floor(this.valueOf()) === this.valueOf()) return 0;
    return this.toString().split(".")[1].length || 0;
}

module.exports = {
    FinancialStateFactory
};


/*
    parametros para generar prioridad entre cuentas
    definidos por la desviacion estandar entre las cuentas

    
*/
