const fs = require('fs')
const path = require('path')
const mime = require('mime')

const _ = require('lodash')
const express = require('express')
var cors = require('cors')
const fileUpload = require('express-fileupload')
const bodyParser = require('body-parser')
const uuid = require('uuid/v1');
const { uploadFile,  extractHeaders , validateFileExtension, extractBody } = require('./common/common')
const { FinancialStateFactory } = require('./common/financial-state.factory')

const app = express()
const port = 3000;

app.use(cors())
app.use(bodyParser.json())
app.use(fileUpload())
app.use('/', express.static(path.join(__dirname, '../public')))
app.use('/files', express.static(path.join(__dirname, '../files')))

app.post('/upload',(request, response) => {
    const file = request.files['uploadFile']

    if(!file) {
        return response.status(400).send({
            status: 'ERR',
            message: 'No files were uploaded'
        })
    }

    let randomString = uuid()
    const getFileHeaders = async () => {
        await validateFileExtension(file, /\.(csv)$/)
        let filePath = await uploadFile(file, randomString)
        let headers = await extractHeaders(filePath)
        return {
            headers: headers,
            filePath: filePath
        }
    }

    getFileHeaders()
    .then((output) => {
        return response.status(200).send({
            status: 'OK',
            message: 'File uploaded',
            headerList: output.headers,
            uuid: randomString
        })
    })
    .catch((err) => {
        console.log(err)
        return response.status(500).send({
            status: 'ERR',
            message: err
        })
    })
})

app.post('/getZipLink',  (request, response) => {
    let options = _.pick(request.body, ['uuid', 'code', 'companyList', 'fileName']);
    console.log(options);
    const financialStateFactory = new FinancialStateFactory()
    const getLinkZip = async () => {
        await financialStateFactory.io({
            uuid: options.uuid,
            code: options.code,
            companyList: options.companyList,
            fileName: options.fileName
        })
        await financialStateFactory.readRawUsingOptions()
        await financialStateFactory.optimize()
        await financialStateFactory.writeCSV()
        await financialStateFactory.generateZIP()
    }

    getLinkZip()
    .then((output) => {
        return response.status(200).send({
            status: 'OK',
            message: 'zip generated'
        })
    }).catch((err) => {
        console.log(err)
        return response.status(500).send({
            status: 'ERR',
            message: err
        })
    })
});

app.get('/getZipByUUID/:uuid/:fileName', function(request, response){
    let options = _.pick(request.params, ['uuid', 'fileName']);
    var file = path.join(__dirname, '/../almanac/', options.uuid, options.fileName + '.zip')
    
    var filename = path.basename(file);
    var mimetype = mime.lookup(file);
    
    response.setHeader('Content-disposition', 'attachment; filename=' + filename);
    response.setHeader('Content-type', mimetype);
    
    var filestream = fs.createReadStream(file);
    filestream.pipe(response);
});

app.listen(port, () => {
    console.log('listening on port', port)
})
