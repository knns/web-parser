import React from 'react';

class CreateTextInput extends React.Component {
    zipNameChange = (zipName) => {
        this.props.handleChangeZipName(zipName.target.value)
    }

    render() {
        return (
            <span>
                <label>
                    {this.props.label}
                </label>
                <input
                    type="text"
                    name={this.props.name}
                    id={this.props.id}
                    onChange={this.zipNameChange}
                />
            </span>
        )
    }
}

export default CreateTextInput;