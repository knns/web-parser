import React from 'react';
import Checkbox from 'material-ui/Checkbox';

class CreateCheckBox extends React.Component {
    state = {
        isChecked: false,
        value: this.props.value
    }
    toggleCheckboxChange = () => {
        this.setState(() => ({isChecked: !this.state.isChecked}));
        let element = {
            value: this.state.value,
            type: 'checkBox'
        };
        if(this.state.isChecked) {
            this.props.handleDeleteToArray(element);
        } else {
            this.props.handleAddToArray(element);
        }
    }
    render() {
        return(
            <span>
                <Checkbox
                    name={this.props.path}
                    value={this.props.value}
                    defaultChecked={this.state.isChecked}
                    onChange={this.toggleCheckboxChange}
                />
                <label hidden={this.props.hidden}>
                    {this.props.label}
                </label>
            </span>
        );
    };
}

export default CreateCheckBox;