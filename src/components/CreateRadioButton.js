import React from 'react';
import Radio from 'material-ui/Radio';

class CreateRadioButton extends React.Component {
    state = {
        value: this.props.value
    }
    toggleInputChange = () => {
        let element = {
            value: this.state.value,
            type: 'radio'
        };
        this.props.handleAddToArray(element);
    }
    render() {
        return(
            <span>
                <Radio
                    name={this.props.path}
                    value={this.props.value}
                    onChange={this.toggleInputChange}
                    disabled={this.props.disabled}
                    hidden={this.props.hidden}
                />
                <label display='inline-block' hidden={this.props.hidden}>
                {this.props.label}
                </label>
            </span>
        );
    };
}

export default CreateRadioButton;