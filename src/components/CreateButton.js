import React from 'react';
import Button from 'material-ui/Button';

class CreateButton extends React.Component {
    render() {
        return (
            <div>
                <Button
                    color={this.props.color}
                    raised={this.props.raised}
                    type={this.props.type}
                    onClick={this.props.action}
                    disabled={this.props.disabled}
                    className={this.props.className}
                >
                    {this.props.text}
                </Button>
            </div>
        );
    };
}

export default CreateButton;