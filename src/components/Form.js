import React from 'react'

import CreateInput from './CreateInput'
import CreateButton from './CreateButton'
import CreateStepper from './CreateStepper'

import Service from './../services/service'

class Form extends React.Component{
    state = {
        headerList: [],
        companyList: [],
        code: undefined,
        fileName: undefined,
        uuid: undefined,
        activeStep: 0,
    };
    handleAddToArray = (element) => {
        if(!element) {
            
        }

        if(element.type === 'radio') {
            this.setState(() => ({ code: element.value }))
        }

        if(element.type === 'checkBox') {
            this.setState((prevState) => ({ companyList: prevState.companyList.concat(element) }))
        }
    }

    handleDeleteToArray = (elementToRemove) => {
        if(elementToRemove.type === 'checkBox') {
            this.setState((prevState) => ({ 
                companyList: prevState.companyList.filter((element) => {
                    return elementToRemove.value !== element.value
                }) 
            }));
        }
    }

    handleAddCompanies = () => {
        this.setState((prevState) => ({ companyList: this.state.array }));
    }

    handleChangeZipName = (newFileName) => {
        this.setState(() => ({ fileName: newFileName }));
    }

    handleGetData = (e) => {
        e.preventDefault();
        const service =  new Service();
        service.getHeadersUsingFormData(
            new FormData(document.getElementById('myform'))
        ).then((response) => {
            this.setState((prevState) => ({ 
                headerList: response.headerList,
                code: undefined,
                uuid: response.uuid,
                activeStep: this.state.activeStep + 1
            }));
        }).catch((err) => {
            console.log(err.response.text)
        })
    }

    handleSendData = () => {
        const service =  new Service()
        
        service.getZipLink({
            uuid: this.state.uuid, 
            code: this.state.code,
            companyList: this.state.companyList.map((company) => {
                return company.value
            }),
            fileName: this.state.fileName 
        }).then((response) => {
            window.open('/getZipByUUID/'+this.state.uuid+'/'+this.state.fileName );
        }).catch((err) => {
            console.log('asd',err)
        })
    }

    handleNext = () => {
        this.setState((prevState) => ({
            activeStep: this.state.activeStep + 1
        }))
    }

    handleBack = () => {
        this.setState((prevState) => ({
            activeStep: this.state.activeStep - 1
        }))
    }

    render() {
        
        return (
            <form id='myform' onSubmit={this.handleGetData}>
                <CreateStepper steps={['Seleccione el archivo .csv',
                    'Seleccione el Codigo de Cuenta',
                    'Seleccione las Empresas',
                    'Generar Zip']} 
                    activeStep={this.state.activeStep} 
                />
                <div hidden={this.state.headerList.length > 0}>
                    <input id="uploadFile" name="uploadFile" type="file" encType="multipart/form-data" />
                    <CreateButton
                        type="button"
                        text="Atras"
                        disabled={true}
                        className="backButton"
                    />
                    <CreateButton
                        type="submit"
                        text="Siguiente"
                        color="primary"
                        raised={true}
                    />
                </div>
                {this.state.headerList.length > 0 &&
                    <div hidden={this.state.activeStep > 1}>
                        <h3>Seleccione Codigo de Cuenta</h3>
                        <CreateInput
                            typeInput="radio"
                            data={this.state.headerList}
                            disabled={this.state.headerList.length == 0}
                            handleAddToArray={this.handleAddToArray}
                            path={"codigo"}
                        />
                        <CreateButton
                            type="button"
                            text="Siguiente"
                            action={this.handleNext}
                            color="primary"
                            raised={true}
                            disabled={this.state.code == undefined}
                        />
                    </div>
                }
                { this.state.activeStep > 1 &&
                    <div hidden={this.state.activeStep > 2}>
                        <h3>Seleccione Empresas:</h3>
                        <CreateInput
                            typeInput="checkBox"
                            data={this.state.headerList}
                            handleAddToArray={this.handleAddToArray}
                            handleDeleteToArray={this.handleDeleteToArray}
                            path={"empresaList"}
                        />
                        <CreateButton
                            type="button"
                            text="Siguiente"
                            action={this.handleNext}
                            color="primary"
                            raised={true}
                            disabled={this.state.companyList.length == 0}
                        />
                    </div>
                }
                <hr/>
                { this.state.activeStep > 2 &&
                    <CreateInput
                        typeInput="text"
                        name="zipName"
                        label="Nombre del archivo zip"
                        id="zipName"
                        handleChangeZipName={this.handleChangeZipName}
                    />
                }
                { this.state.fileName !== undefined && 
                    <CreateButton
                        type="button"
                        text="Generar Zip"
                        action={this.handleSendData}
                        color="primary"
                        raised={true}
                    />
                }
            </form>
        );
    };
}

export default Form;
