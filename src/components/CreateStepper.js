import React from 'react'
import Stepper, { Step, StepLabel } from 'material-ui/Stepper'

class CreateStepper extends React.Component {
    state = {
        getSteps: this.props.steps
    }

    render() {
        return(
            <div>
                <Stepper activeStep={this.props.activeStep} alternativeLabel>
                    {this.state.getSteps.map((step) => {
                        return (
                            <Step key={step}>
                                <StepLabel>{step}</StepLabel>
                            </Step>
                        )
                    })}
                </Stepper>
            </div>
        )
    }
}

export default CreateStepper