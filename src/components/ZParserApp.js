import React from 'react';
import Header from './Header';
import Form from './Form';

const ZParserApp = () => (
    <div>
        <Header title={'Z Parser App'} />
        <Form />
    </div>   
);

export default ZParserApp;