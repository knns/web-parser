import React from 'react';
import CreateCheckBox from './CreateCheckBox';
import CreateRadioButton from './CreateRadioButton'
import CreateTextInput from './CreateTextInput'

class CreateInput extends React.Component {
    render() {
        return (
            <div>
                {
                    this.props.typeInput != "text" &&
                    this.props.data.map((element) => {
                        if(this.props.typeInput === "radio") {
                            return (
                                <CreateRadioButton
                                    label={element}
                                    key={element}
                                    value={element}
                                    handleAddToArray={this.props.handleAddToArray}
                                    disabled={this.props.disabled}
                                    hidden={this.props.hidden}
                                    path={this.props.path}
                                />
                            )
                        }

                        if(this.props.typeInput === "checkBox") {
                            return (
                                <CreateCheckBox
                                    label={element}
                                    key={element}
                                    value={element}
                                    handleAddToArray={this.props.handleAddToArray}
                                    handleDeleteToArray={this.props.handleDeleteToArray}
                                    disabled={this.props.disabled}
                                    hidden={this.props.hidden}
                                />
                            )
                        }
                    })
                }
                {this.props.typeInput === "text" &&
                    <CreateTextInput 
                        name={this.props.name}
                        label={this.props.label}
                        id={this.props.id}
                        handleChangeZipName={this.props.handleChangeZipName}
                    />
                }
            </div>
        );
    };
}
    

export default CreateInput;