import React from 'react'
import ReactDOM from 'react-dom'
import ZParserApp from './components/ZParserApp';

import './styles/styles.scss';

ReactDOM.render(<ZParserApp />, document.getElementById('app'))