import request from 'superagent'
//import fs from 'fs'

export default class Service {
    constructor() {

    }
    getHeadersUsingFormData(formData) {
        return new Promise((resolve, reject) => {
            request.post(
                'upload/'
            ).send(
                formData
            ).end((err, response) => {
                if(err) {
                    reject(err)
                } else {
                    resolve(response.body)
                }
            });
        })
    }

    getZipLink(object) {
        return new Promise((resolve, reject) => {
            request.post(
                'getZipLink/'
            ).send({
                uuid: object.uuid,
                code: object.code,
                companyList: object.companyList,
                fileName: object.fileName
            })
            .end((err, response) => {
                if(err) {
                    reject(err) 
                } else {
                    resolve(response)
                }
            });
        })
    }
}
